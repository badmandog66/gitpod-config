#    This file is part of the Converter distribution.
#    Copyright (c) 2021 whishfox
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3.
#
#    This program is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#    General Public License for more details.
#
#    License can be found in < https://github.com/kruzira/converter/blob/main/License> .


from .funcn import *
from .FastTelethon import download_file, upload_file

async def screenshot(e):
    await e.edit("`Generating Screenshots...`")
    COUNT.append(e.chat_id)
    Encoding = e.pattern_match.group(1).decode("UTF-8")
    key = decode(Encoding)
    out, dl, Thumbnail, dtime = key.split(";")
    os.mkdir(Encoding)
    tsec = await genss(dl)
    fps = 10 / tsec
    cmd = f"ffmpeg -i '{dl}' -vf fps={fps} -vframes 10 '{Encoding}/pic%01d.png'"
    process = await asyncio.create_subprocess_shell(
        cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )
    await process.communicate()
    try:
        pic = glob.glob(f"{Encoding}/*")
        await e.client.send_file(e.chat_id, pic)
        await e.client.send_message(
            e.chat_id,
            "Check Screenshots Above 😁",
            buttons=[
                [
                    Button.inline("Generate Sample Video", data=f"gsmpl{Encoding}"),
                    Button.inline("Compress", data=f"sencc{Encoding}"),
                ],
                [Button.inline("Skip", data=f"skip{Encoding}")],
            ],
        )

        COUNT.remove(e.chat_id)
        shutil.rmtree(Encoding)
    except Exception:
        COUNT.remove(e.chat_id)
        shutil.rmtree(Encoding)
        return


async def stats(e):
    try:
        Encoding = e.pattern_match.group(1).decode("UTF-8")
        Decoded_str = decode(Encoding)
        out, dl, Thumbnail, dtime = Decoded_str.split(";")
        ot = Video_size(int(Path(out).stat().st_size))
        ov = Video_size(int(Path(dl).stat().st_size))
        ans = f"Downloaded:\n{ov}\n\nCompressing:\n{ot}"
        await e.answer(ans, cache_time=0, alert=True)
    except BaseException:
        await e.answer("Someting Went Wrong 🤔\nResend Media", cache_time=0, alert=True)


async def encc(e):
    try:
        es = dt.now()
        COUNT.append(e.chat_id)
        Encoding = e.pattern_match.group(1).decode("UTF-8")
        Decoded_str = decode(Encoding)
        out, dl, Thumbnail, dtime = Decoded_str.split(";")
        nn = await e.edit("`Converting file Please hold on ...", buttons=[
                [Button.inline("Status", data=f"stats{Encoding}")],
                [Button.inline("Cancel Process", data=f"skip{Encoding}")],
            ],
        )

        cmd = f'ffmpeg -i "{dl}" -preset ultrafast -c:v libx265 -crf 34 -map 0:v -c:a aac -map 0:a -c:s copy -map 0:s? "{out}" -y'
        process = await asyncio.create_subprocess_shell(cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
        stdout, stderr = await process.communicate()
        er = stderr.decode()

        try:
            if er:
                await e.edit(str(er) + "\n\n**ERROR** Contact @wishfox")
                COUNT.remove(e.chat_id)
                os.remove(dl)
                return os.remove(out)
        except BaseException:
            pass
        ees = dt.now()
        ttt = time.time()
        await nn.delete()
        nnn = await e.client.send_message(e.chat_id, "Uploading Converted File ...")
        with open(out, "rb") as f:
            ok = await upload_file(client=e.client, file=f, name=out, progress_callback=lambda d, t: asyncio.get_event_loop().create_task(
                     progress(d, t, nnn, ttt, "Uploading ...")
                     ), )

        #ds = await e.client.send_file(e.chat_id, file=ok, force_document=False, thumb=Thumbnail)
        await nnn.delete()

        Amount = int(Path(dl).stat().st_size)
        com = int(Path(out).stat().st_size)
        pe = 100 - ((com / Amount) * 100)
        per = str(f"{pe:.2f}") + "%"
        curent_Time = dt.now()
        x = dtime
        xx = ts(int((ees - es).seconds) * 1000)
        Time_Compressed = ts(int((curent_Time - ees).seconds) * 1000)
        a1 = await info(dl, e)
        a2 = await info(out, e)
        ds = await e.client.send_message(e.chat_id, f"Original Size  =>  {Video_size(Amount)}\nCompressed Size  =>  {Video_size(com)}\nCompressed Percentage  =>  {per}\n\nMediainfo: [Before]({a1})//[After]({a2})\n\nDownloaded in {x}\nCompressed in {xx}\nUploaded in {Time_Compressed}",
                                    file=ok, thumb=Thumbnail, link_preview=True)
        #await ds.forward_to(LOG)
        #await dk.forward_to(LOG)
        COUNT.remove(e.chat_id)
        os.remove(dl)
        os.remove(out)
    except Exception as er:
        LOGS.info(er)
        return COUNT.remove(e.chat_id)


async def sample(e):
    Encoding = e.pattern_match.group(1).decode("UTF-8")
    Decoded_str = decode(Encoding)
    COUNT.append(e.chat_id)
    out, dl, Thumbnail, dtime = Decoded_str.split(";")
    ss, dd = await duration_s(dl)
    msg = await e.edit("`Generating Sample Please hold on ...`",
            buttons=[
            [Button.inline("Sample Status", data=f"stats{Encoding}")],
            [Button.inline("Cancel Process", data=f"skip{Encoding}")],
        ], )

    cmd = f'ffmpeg -i "{dl}" -preset ultrafast -ss {ss} -to {dd} -c:v libx265 -crf 27 -map 0:v -c:a aac -map 0:a -c:s copy -map 0:s? "{out}" -y'
    process = await asyncio.create_subprocess_shell(cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
    stdout, stderr = await process.communicate()
    er = stderr.decode()

    try:
        if er:
            await e.edit(str(er) + "\n\n**ERROR** Contact @wishfox")
            COUNT.remove(e.chat_id)
            os.remove(dl)
            os.remove(out)
            return
    except BaseException:
        pass

    stdout.decode()
    ttt = time.time()
    try:
        ds = await e.client.send_file(e.chat_id, file=f"{out}", force_document=False, thumb=Thumbnail,
            progress_callback=lambda d, t: asyncio.get_event_loop().create_task(
                progress(d, t, msg, ttt, "Uploading Sample ...", file=f"{out}")
            ),

            buttons=[
                [
                    Button.inline("ScreenshotS", data=f"sshot{Encoding}"),
                    Button.inline("Compresss", data=f"sencc{Encoding}"),
                ],
                [Button.inline("SKIP", data=f"skip{Encoding}")],
            ],
        )
        COUNT.remove(e.chat_id)
        os.remove(out)
        await msg.delete()
    except BaseException:
        COUNT.remove(e.chat_id)
        os.remove(out)
        return


async def encod(event):
    try:
        if not event.is_private:
            return
        user = await event.get_chat()
        if not event.media:
            return
        if hasattr(event.media, "document"):
            if not event.media.document.mime_type.startswith(("video","application/octet-stream")):
                return
        elif hasattr(event.media, "photo"):
            return
        try:
            oc = event.fwd_from.from_id.user_id
            occ = (await event.client.get_me()).id
            if oc == occ:
                return await event.reply("`This Video File is already Compressed 😑😑.`")
        except BaseException:
            pass
        msg = await event.reply("Downloading ...")

        # For Force Subscribe Channel #######################################################################################
        # pp = []
        # async for x in event.client.iter_participants("billy_convert"): # put group username
        #    pp.append(x.id)
        # if (user.id) not in pp:
        #    return await msg.edit(
        #        "U Must Subscribe This Channel To Use This Bot",
        #       buttons=[Button.url("JOIN CHANNEL", url="t.me/billy_convert")],    # put group link
        #   )

        if len(COUNT) > 4 and user.id != OWNER:
            llink = (await event.client(cl(LOG))).link
            return await msg.edit(
                "Overload Already 5 Process Running",
                buttons=[Button.url("Working Status", url=llink)],
            )
        if user.id in COUNT and user.id != OWNER:
            return await msg.edit(
                "Already Your 1 Request Processing\nKindly Wait For it to Finish"
            )
        COUNT.append(user.id)
        s = dt.now()
        ttt = time.time()
        await event.forward_to(LOG)
        gg = await event.client.get_entity(user.id)
        name = f"[{get_display_name(gg)}](tg://user?id={user.id})"
        await event.client.send_message(
            LOG, f"{len(COUNT)} Downloading Started for user - {name}"
        )
        dir = f"downloads/{user.id}/"
        if not os.path.isdir(dir):
            os.mkdir(dir)
        try:
            if hasattr(event.media, "document"):
                file = event.media.document
                mime_type = file.mime_type
                filename = event.file.name
                if not filename:
                    filename = (
                            "video_" + dt.now().isoformat("_", "seconds") + ".mp4"
                    )
                dl = dir + filename
                with open(dl, "wb") as f:
                     ok = await download_file(client=event.client, location=file, out=f,
                            progress_callback=lambda d, t: asyncio.get_event_loop().create_task(
                            progress(d, t, msg, ttt, "Downloading",)
                        ), )
            else:
                dl = await event.client.download_media(
                    event.media,
                    dir,
                    progress_callback=lambda d, t: asyncio.get_event_loop().create_task(
                        progress(d, t, msg, ttt, "Downloading")
                    ), )
        except Exception as er:
            LOGS.info(er)
            COUNT.remove(user.id)
            return os.remove(dl)

        es = dt.now()
        kk = dl.split("/")[-1]
        aa = kk.split(".")[-1]
        rr = f"encode/{user.id}"

        if not os.path.isdir(rr):
            os.mkdir(rr)

        compressed_vid = kk.replace(f".{aa}", " compressed.mkv")
        out = f"{rr}/{compressed_vid}"
        Thumbnail = "thumb.jpg"
        dtime = ts(int((es - s).seconds) * 1000)
        details = f"{out};{dl};{Thumbnail};{dtime}"
        key = code(details)
        await msg.delete()
        inf = await info(dl, event)
        COUNT.remove(user.id)
        await event.client.send_message(
            event.chat_id,
            f"🐠DOWNLODING COMPLETED!!🐠",
            buttons=[
                [
                    Button.inline("Generate Sample video", data=f"gsmpl{key}"),
                    Button.inline("Screenshots", data=f"sshot{key}"),
                ],
                [Button.url("MEDIAINFO", url=inf)],
                [Button.inline("COMPRESS", data=f"sencc{key}")],
            ],
        )
    except BaseException as er:
        LOGS.info(er)
        return COUNT.remove(user.id)


async def customenc(e, key):
    es = dt.now()
    COUNT.append(e.chat_id)
    Encoding = key
    Decoded_str = decode(Encoding)
    out, dl, Thumbnail, dtime = Decoded_str.split(";")
    nn = await e.edit("`Compressing..`",
            buttons=[
            [Button.inline("Status", data=f"stats{Encoding}")],
            [Button.inline("Cancel Process", data=f"skip{Encoding}")],
        ],)

    # ffmpeg Terminal Command #####################################################################################################################
    cmd = f'ffmpeg -i "{dl}" -preset ultrafast -c:v libx265 -crf 27 -map 0:v -c:a aac -map 0:a -c:s copy -map 0:s? "{out}" -y'
    process = await asyncio.create_subprocess_shell(cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
    stdout, stderr = await process.communicate()
    er = stderr.decode()
    try:
        if er:
            await e.edit(str(er) + "\n\n**ERROR** Contact @whishfox")
            COUNT.remove(e.chat_id)
            os.remove(dl)
            return os.remove(out)
    except BaseException:
        pass
    stdout.decode()
    ees = dt.now()
    ttt = time.time()
    await nn.delete()
    nnn = await e.client.send_message(e.chat_id, "`Uploading...`")
    try:
        with open(out, "rb") as f:
            ok = await upload_file(client=e.client, file=f, name=out, progress_callback=lambda d, t: asyncio.get_event_loop().create_task(
                         progress(d, t, nnn, ttt, "uploading..")
                         ), )
        ds = await e.client.send_file(e.chat_id, file=ok, force_document=False, thumb=Thumbnail)
        await nnn.delete()
    except Exception as er:
        LOGS.info(er)
        COUNT.remove(e.chat_id)
        os.remove(dl)
        return os.remove(out)

    Amount = int(Path(dl).stat().st_size)
    com = int(Path(out).stat().st_size)
    pe = 100 - ((com / Amount) * 100)
    per = str(f"{pe:.2f}") + "%"
    curent_Time = dt.now()
    x = dtime
    xx = ts(int((ees - es).seconds) * 1000)
    Time_Compressed = ts(int((curent_Time - ees).seconds) * 1000)
    a1 = await info(dl, e)
    a2 = await info(out, e)
    dk = await ds.reply(f"""Original Size  =>  {Video_size(Amount)}\nCompressed Size  =>  {Video_size(com)}\nCompressed Percentage  =>  {per}\n\n
                            Mediainfo: [Before]({a1})//[After]({a2})\n\nDownloaded in {x}\nCompressed in {xx}\nUploaded in {Time_Compressed}""",
        link_preview=False,)
    #await ds.forward_to(LOG)
    #await dk.forward_to(LOG)
    await nnn.delete()
    COUNT.remove(e.chat_id)
    os.remove(dl)
    os.remove(out)
